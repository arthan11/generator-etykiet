# -*- coding: utf-8 -*-
from os.path import exists
from PIL import Image, ImageDraw, ImageFont
from reportlab.pdfgen.canvas import Canvas
from xml.dom import minidom

class PixelCounter(object):
    ''' loop through each pixel and average rgb '''

    def __init__(self, imageName):
        self.pic = Image.open(imageName)
        # load image data
        self.imgData = self.pic.load()

    def averagePixels(self):
        r, g, b = 0, 0, 0
        count = 0
        for x in xrange(self.pic.size[0]):
            for y in xrange(self.pic.size[1]):
                clrs = self.imgData[x,y]
                r += clrs[0]
                g += clrs[1]
                b += clrs[2]
                count += 1
        # calculate averages
        return (r/count), (g/count), (b/count), count

class LabelGenerator(object):
    def __init__(self):
        self.W = 709
        self.H = 551
        self.DPI = 400
        self.R = float(self.W)/float(self.H)
        self.img = None
        self.name = None
        self.template = 'oval'
        self.font = 'Ginko'
        self.font_color = None
        self.txt_max_w = 600
        self.txt_max_h = 120
        self.gen_dir = 'gen'

        self.margin_bottom = 50
        self.margin_left = 20

        self.fonts = ()
        self.templates = ()
        self.items_names = ()

    def generate(self, name):
        self.name =  name.decode('utf-8')
        back = u'backgrounds\\{}.png'.format(self.name)
        DOMTree = minidom.parse('{}.xml'.format(self.template))



        if self.font_color == None:
          pc = PixelCounter(back)
          aver = pc.averagePixels()
          self.font_color = (aver[0], aver[1], aver[2])

        self.img = Image.new("RGBA",(self.W,self.H), "white")

        background = Image.open(back)
        b_w, b_h = background.size
        b_r = float(b_w)/float(b_h)
        if b_r > self.R:
            new_h = self.H
            new_w = int(round( float(b_w) * (float(new_h)/float(b_h) )))
        else:
            new_w = self.W
            new_h = int(round( float(b_h) * (float(new_w)/float(b_w) )))

        background = background.resize((new_w, new_h), Image.ANTIALIAS)
        self.img.paste(background, (0, 0))
        foreground = Image.open('templates\\{}.png'.format(self.template))
        self.img.paste(foreground, (0, 0), foreground)

        draw = ImageDraw.Draw(self.img)
        font_size = 18
        w, h = 0, 0
        while w <= self.txt_max_w and h <= self.txt_max_h:
            font_size += 1
            font = ImageFont.truetype('fonts\\{}.ttf'.format(self.font), font_size)
            w, h = draw.textsize(self.name, font=font)
        font_size -= 1
        draw.text(((self.W-w)/2,(self.H-h)/2), self.name, fill=self.font_color, font=font)

        #self.img.show()

    def save(self, filename):
        self.img.save(filename, 'PNG', dpi=(float(self.DPI), float(self.DPI)))

    def generate_all(self):
        for template in self.templates:
            self.template = template
            for font in self.fonts:
                self.font = font
                for item_name in self.items_names:
                    filename = '{}\\{}_{}_{}.png'.format(self.gen_dir, item_name, template, font).decode('utf-8')
                    if not exists(filename):
                        print filename
                        gen.generate(item_name)
                        gen.save(filename)
        print 'przypraw: {}'.format(len(self.items_names))

    def generate_pdf(self, filename):
        c = Canvas(filename)
        x = self.margin_left
        y = self.margin_bottom
        w = 128 # !! CALC !!
        h = 99 # !! CALC !!
        max_per_line = 4 # CALC ??
        max_per_page = 28 # CALC ??

        nr = 0
        page = 1
        print 'page {}'.format(page)
        for item_name in self.items_names:
            for template in self.templates:
                for font in self.fonts:
                    if nr > 0:
                        if (nr-1) % max_per_page == max_per_page - 1:
                            c.showPage()
                            y = self.margin_bottom
                            page += 1
                            print 'page {}'.format(page)

                    filename = '{}\\{}_{}_{}.png'.format(self.gen_dir, item_name, template, font).decode('utf-8')
                    #print item_name, template, font
                    if exists(filename):
                        c.drawImage(filename, x, y, width=w, height=h)
                        if nr % max_per_line == max_per_line-1:
                            x = self.margin_left
                            y = y + h + 10
                        else:
                            x = x + w + 10
                        nr += 1
        print 'saving...'
        c.save()
        print 'done.'


if __name__ == '__main__':

    przyprawy = (
        'bazylia',
        'chili',
        'curry',
        'cynamon',
        'estragon',
        'gałka muszkatołowa',
        'goździki',
        'imbir',
        'kardamon',
        'kminek',
        'kolendra',
        'kurkuma mielona',
        'majeranek',
        'natka pietruszki',
        'liść laurowy',
        'oregano',
        'papryka ostra',
        'papryka słodka',
        'pieprz biały',
        'pieprz cytrynowy',
        'pieprz czarny mielony',
        'pieprz kolorowy',
        'przyprawa do grilla',
        'przyprawa do kurczaka',
        'rodzynki',
        'siemię lniane',
        'sól himalajska',
        'tymianek',
        'ziele angielskie',
        'zioła prowansalskie',
    )

    fonts = (
        'AfaratibnBlady',
        'Electroharmonix',
        'EthnocentricRg',
        'Ginko',
        'StarNext',
        'YoureGone',
    )

    templates = (
        'oval',
        'rectangle',
        'old',
    )

    gen = LabelGenerator()
    gen.templates = templates
    gen.fonts = fonts
    gen.items_names = [przyprawy[0]]

    gen.W = 619
    gen.H = 354
    gen.font_color = (0, 0, 0)
    gen.txt_max_w = 360
    gen.txt_max_h = 130
    gen.generate_all()

    #gen.generate_pdf('etykiety.pdf')

